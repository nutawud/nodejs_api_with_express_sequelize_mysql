module.exports = app => {
  const user = require("../controllers/user.controller.js");

  var router = require("express").Router();

  // Create a new Tutorial
  router.post("/login", user.login);
  router.post("/register", user.register);

  app.use('/api/user', router);
};
