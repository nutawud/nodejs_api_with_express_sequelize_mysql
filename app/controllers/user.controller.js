const db = require("../models");
const User = db.user;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require("../config/auth.config");

exports.login = (req, res) => {

  const { email = '', password } = req.body;
  User.findOne({
    where: {
      email: email
    }
  })
    .then((user) => {
      return bcrypt.compare(password, user.password)
        .then((result) => {
          if (!result) {
            res.status(401)
              .json({
                message: "Authentication failed"
              })
          } else {
            let jwtToken = jwt.sign({
              userId: user.id
            },
              config.secret, {
              expiresIn: "60s"
            });
            res.status(200).json({
              status: "OK",
              data: user,
              token: jwtToken,
              expiresIn: '60s',
            });
          }
        }).catch((error) => {
          res.status(401)
            .json({
              message: "Authentication failed",
              error: error
            })
        })

    })
    .catch((error) => {
      res.status(500)
        .json({
          message: error
        })
    })
}

exports.register = (req, res, next) => {

  User.create({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  })
    .then(user => {
      res.send({ message: "User was registered successfully!" });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
}